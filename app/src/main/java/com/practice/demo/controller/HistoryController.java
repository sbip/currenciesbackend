package com.practice.demo.controller;

import com.practice.demo.model.History;
import com.practice.demo.service.HistoryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/history")
public class HistoryController {

    private final HistoryService historyService;

    @GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('USER')")
    public List<History> getHistory() {
        log.info("HistoryController's getHistory method called");

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return historyService.getHistory(authentication.getName());


    }
}
