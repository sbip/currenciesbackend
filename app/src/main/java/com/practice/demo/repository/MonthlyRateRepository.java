package com.practice.demo.repository;

import com.practice.demo.model.MonthlyRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonthlyRateRepository extends JpaRepository<MonthlyRate, Long> {
}
