package com.practice.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@Entity
@Table(name = "monthly_rate")
public class MonthlyRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "currency")
    private String currency;

    @Column(name = "average_rate")
    private BigDecimal averageRate;

    @Column(name = "date")
    private LocalDate date;

    public MonthlyRate(String currency, BigDecimal averageRate, LocalDate date) {
        this.currency = currency;
        this.averageRate = averageRate;
        this.date = date;
    }
}


