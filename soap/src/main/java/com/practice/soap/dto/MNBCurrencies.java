package com.practice.soap.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MNBCurrencies {

    @JsonProperty("FirstDate")
    FirstDate firstDate;
    @JsonProperty("LastDate")
    LastDate lastDate;
    @JacksonXmlElementWrapper(localName = "Currencies")
    List<Curr> currencies = new ArrayList<>();


}
