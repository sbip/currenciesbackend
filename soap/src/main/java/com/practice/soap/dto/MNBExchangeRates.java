package com.practice.soap.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Data;

import java.util.List;

@Data
public class MNBExchangeRates {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("Day")
    List<Day> day;
}
